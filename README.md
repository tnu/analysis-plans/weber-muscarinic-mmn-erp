Lilian Weber

This analysis plan describes all analysis steps for the sensor-space analysis of
the EEG data recorded during the auditory mismatch paradigm in the antagonist 
arm of the DPRST study. In this project, we use both classical ERP analyses as 
well as recently established single-trial ERP analyses based on a computational 
model to characterise dopaminergic and cholinergic processes during an auditory 
mismatch paradigm, using EEG under pharmacological interventions (between 
subject design). The paradigm has been designed to examine auditory inference in
a volatile environment. The project includes 81 healthy male participants.

Contributions to the analysis plan: Lilian Weber, Klaas Enno Stephan